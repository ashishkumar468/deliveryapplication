package com.Ulink.Agrostar.deliverystatus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class SplashScreenActivity extends Activity {
	private static final int SPLASH_DISPLAY_TIME = 6000; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		new Handler().postDelayed(new Runnable() {
	        @Override 
	        public void run()
	        {
	            Intent intent = new Intent(SplashScreenActivity.this, DeliveryStatusLoginActivity.class);
	        startActivity(intent); 
	        SplashScreenActivity.this.finish(); 
	        overridePendingTransition(R.layout.splashfadein, R.layout.activityfadeout); }
	        }, SPLASH_DISPLAY_TIME);
		Intent intent = new Intent(SplashScreenActivity.this, DeliveryStatusLoginActivity.class);
		startActivity(intent);
	}

	
}
