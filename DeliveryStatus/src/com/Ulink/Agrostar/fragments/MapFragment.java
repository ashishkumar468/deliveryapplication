package com.Ulink.Agrostar.fragments;

import com.Ulink.Agrostar.deliverystatus.ManagerActivity;

import com.Ulink.Agrostar.deliverystatus.R;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

 
public class MapFragment extends Fragment{
	
    public MapFragment(){}
    MapView mMapView;
     
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
  
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
         
    
 
    
          
        return rootView;
    }
   
}

