package com.ulink.agrostar.deliveryapplication_v1.request;





import java.util.ArrayList;

import com.ulink.agrostar.deliveryapplication_v1.models.CallData;

public class CallingDataReqPojo {

	ArrayList<CallData> listData = new ArrayList<CallData>();

	public ArrayList<CallData> getListData() {
		return listData;
	}

	public void setListData(ArrayList<CallData> listData) {
		this.listData = listData;
	}

}
