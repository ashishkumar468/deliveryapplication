package com.ulink.agrostar.deliveryapplication_v1.common;


import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import com.ulink.agrostar.deliveryapplicationv1.utils.*;

public class CommonOperations {

	private static Dialog dialog;
	static ProgressBar horProgressBar;

	/*
	 * DDMMYYY format converter
	 */
	
	/*
	 * Is Internet Connectivity exist
	 */
	public static boolean isNetworkAvailable(Context _Context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) _Context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	
	public static String prepareRequest(MethodParamsWrapper wrapper2,
			RequestParamBase base) {

		Gson gson = new Gson();
		Wrapper wrapper = new Wrapper();
		wrapper.setRequest(base);
		wrapper.setMethodparam(wrapper2);
		return gson.toJson(wrapper);

	}

	
	
}
