package com.ulink.agrostar.deliveryapplicationv1.utils;

public class Wrapper {

	private Object request;
	private MethodParamsWrapper methodparam;

	public Object getRequest() {
		return request;
	}

	public void setRequest(Object request) {
		this.request = request;
	}

	public MethodParamsWrapper getMethodparam() {
		return methodparam;
	}

	public void setMethodparam(MethodParamsWrapper methodparam) {
		this.methodparam = methodparam;
	}

}