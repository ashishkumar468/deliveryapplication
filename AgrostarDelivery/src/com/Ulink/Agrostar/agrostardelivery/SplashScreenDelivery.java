package com.Ulink.Agrostar.agrostardelivery;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;


public class SplashScreenDelivery extends Activity {
     
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);    
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
		setContentView(R.layout.activity_splash_screen_delivery);
		
	
		

		Thread timer = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO sleep application for particular duration
				try {

					
						Thread.sleep(3000);
						
						Intent intent=new Intent(SplashScreenDelivery.this,LoginActivityDelivery.class);
						startActivity(intent);
						
				
					}
					
				
				catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		});

		timer.start();
		
	

	}
	
}
