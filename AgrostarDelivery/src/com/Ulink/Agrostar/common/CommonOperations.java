package com.Ulink.Agrostar.common;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import com.ulink.agrostar.util.MethodParamsWrapper;
import com.ulink.agrostar.util.RequestParamBase;
import com.ulink.agrostar.util.Wrapper;

public class CommonOperations {

	private static Dialog dialog;
	static ProgressBar horProgressBar;

	/*
	 * DDMMYYY format converter
	 */
	
	/*
	 * Is Internet Connectivity exist
	 */
	public static boolean isNetworkAvailable(Context _Context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) _Context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	/*
	 * Dialogue Yes & No
	 */
	
	/*
	 * Dialogue OK button
	 */
	
	/*
	 * Close Progress bar
	 */
	public static void closeProgress() {
		try {
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/*
	 * Request creator for network call
	 */
	public static String prepareRequest(MethodParamsWrapper wrapper2,
			RequestParamBase base) {

		Gson gson = new Gson();
		Wrapper wrapper = new Wrapper();
		wrapper.setRequest(base);
		wrapper.setMethodparam(wrapper2);
		return gson.toJson(wrapper);

	}

	
	
}
