package com.Ulink.Agrostar.fragments;

import com.Ulink.Agrostar.agrostardelivery.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CatalogueFragment extends Fragment{
public CatalogueFragment(){}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
  
        View rootView = inflater.inflate(R.layout.catalogue_fragment, container, false);
          
        return rootView;
    }
}
