package com.ulink.agrostar.deliveryapplication.locationupdates;

import com.google.android.gms.location.LocationRequest;

import android.os.Bundle;

public class getLocationUpdates {

	protected void createLocationRequest() {
	    LocationRequest mLocationRequest = new LocationRequest();
	    mLocationRequest.setInterval(10000);
	    mLocationRequest.setFastestInterval(5000);
	    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}
	
	
}
