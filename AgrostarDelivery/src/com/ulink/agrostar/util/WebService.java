package com.ulink.agrostar.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.protocol.HTTP;

import android.app.Application;
import android.util.Log;

public class WebService extends Application {

	// default webservice request format

	private String _apiUrl;
	private final static int TIMEOUT_MILLISEC = 40000;

	public void setApiUrl(String _apiUrl) {
		this._apiUrl = _apiUrl;
	}

	public WebService(String apiUrl) {
		setApiUrl(apiUrl);
	}

	public String excutePost(String urlParameters) {
		URL url;
		HttpURLConnection connection = null;
		try {

			// Create connection
			urlParameters = "REQUEST=" + urlParameters;

			Log.e("WebServiceRequest", urlParameters);

			url = new URL(_apiUrl);

			Log.e("WebServiceIP", url + "");
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setConnectTimeout(TIMEOUT_MILLISEC);
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			Log.e("WebServiceResponse", response.toString());

			return response.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return null;

		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
	}

}
