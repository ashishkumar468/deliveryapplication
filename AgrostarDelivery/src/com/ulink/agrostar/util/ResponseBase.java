package com.ulink.agrostar.util;

public class ResponseBase {

	/**
	 * response code and other details
	 */
	private String RESPONSE;
	private String CODE;
	private String DETAIL;

	public String getRESPONSE() {
		return RESPONSE;
	}

	public void setRESPONSE(String rESPONSE) {
		RESPONSE = rESPONSE;
	}

	public String getCODE() {
		return CODE;
	}

	public void setCODE(String cODE) {
		CODE = cODE;
	}

	public String getDETAIL() {
		return DETAIL;
	}

	public void setDETAIL(String dETAIL) {
		DETAIL = dETAIL;
	}

}
